#!/usr/bin/env bash

# make executable: chmod u+x deploy.sh

read -p "Enter environment: " environment
read -p "Enter username: " username
read -s -p "Enter Password: " password

mvn apigee-enterprise:deploy -P $environment -Dusername=$username -Dpassword=$password
package com.rccl.middleware.ai.exception;

/**
 * Custom Exception class.
 */
public class AIVideoAnalyticException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor with one param.
     * @param message error message
     */
    public AIVideoAnalyticException(String message) {
        super(message);
    }

    /**
     * Constructor with two param.
     * @param message error message
     * @param cause error cause
     */
    public AIVideoAnalyticException(String message, Throwable cause) {
        super(message, cause);
    }
}

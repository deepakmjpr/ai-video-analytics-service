package com.rccl.middleware.ai.data.access;

import java.util.List;
import java.util.Map;

import com.rccl.middleware.ai.exception.AIVideoAnalyticException;

public interface CommonDataAccess {

    List getByQuery(String query, Map<String, Object> parameters) throws AIVideoAnalyticException;
}

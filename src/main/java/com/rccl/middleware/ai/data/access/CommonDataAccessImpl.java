package com.rccl.middleware.ai.data.access;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.rccl.middleware.ai.common.Constant;
import com.rccl.middleware.ai.exception.AIVideoAnalyticException;

/**
 * Data access implementing class.
 */
@Repository
@Transactional
public class CommonDataAccessImpl implements CommonDataAccess {

    private static Logger logger = LoggerFactory.getLogger(CommonDataAccessImpl.class);

    @PersistenceUnit
    EntityManagerFactory entityManagerFactory;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List getByQuery(String hqlQuery, Map<String, Object> parameters) throws AIVideoAnalyticException {

        try {
            if (StringUtils.isEmpty(hqlQuery)) {
                throw new AIVideoAnalyticException(Constant.QUERY_NOT_EMPTY);
            }

            Query query = entityManager.createQuery(hqlQuery);
            if (!CollectionUtils.isEmpty(parameters)) {
                for (Map.Entry<String, Object> param : parameters.entrySet()) {
                    query.setParameter(param.getKey(), param.getValue());
                }
            }
            return query.getResultList();
        } catch (RuntimeException e) {
            logger.error(e.getMessage(), e);
            throw new AIVideoAnalyticException(e.getMessage(), e);
        }
    }
}

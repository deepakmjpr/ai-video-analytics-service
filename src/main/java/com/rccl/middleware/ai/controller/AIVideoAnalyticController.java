package com.rccl.middleware.ai.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.rccl.middleware.ai.service.AIVideoAnalyticService;

@RestController
@ControllerAdvice
public class AIVideoAnalyticController {

    private static Logger logger = LoggerFactory.getLogger(AIVideoAnalyticController.class);
    
    @Autowired
    private AIVideoAnalyticService aiVideoAnalyticService;

    @GetMapping(value = "/last_results/venue/{venue}/camera_id/{camera_id}", produces = "application/json")
    public ResponseEntity<Object> getAveragePeopleCount(@PathVariable("venue") String venue,
            @PathVariable("camera_id") String cameraId) {

        Map<String, Object> averagePeopleCount = aiVideoAnalyticService.getAveragePeopleCount(venue, cameraId);
        logger.info("Average People Count: {}", averagePeopleCount);
        return new ResponseEntity<>(averagePeopleCount, HttpStatus.OK);
    }
}

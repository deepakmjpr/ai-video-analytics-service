package com.rccl.middleware.ai.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.rccl.middleware.ai.model.ApplicationConfiguration;
import com.rccl.middleware.ai.model.AvailabilityResponse;
import com.rccl.middleware.ai.model.ErrorDetails;
import com.rccl.middleware.ai.model.ErrorDetailsFactory;
import com.rccl.middleware.ai.model.ValidationError;
import com.rccl.middleware.ai.service.ConfigService;

@ControllerAdvice
@RestController
public class AiVideoTestController extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(AiVideoTestController.class);
    
    @Autowired
    private ConfigService configService;

    @GetMapping(value = "/health", produces = "application/json")
    public ResponseEntity<Object> getHealth() {
        // do you want to do some logging
        return new ResponseEntity<>("test", new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping(value = "/config", produces = "application/json")
    public ResponseEntity<Object> getConfig() {
        ApplicationConfiguration config = configService.getConfig();
        return new ResponseEntity<>(config, HttpStatus.OK);
    }

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {

        AvailabilityResponse response = new AvailabilityResponse();
        response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.toString());
        ErrorDetails errorDetails = ErrorDetailsFactory.buildInvalidParametErrorDetails();

        List<ObjectError> errors = ex.getBindingResult().getAllErrors();
        for (ObjectError error : errors) {
            if (error instanceof FieldError) {
                FieldError fieldError = (FieldError) error;
                String rejectedValue = "null";
                if (fieldError.getRejectedValue() != null) {
                    rejectedValue = fieldError.getRejectedValue().toString();
                }
                errorDetails.getValidationErrors()
                        .add(new ValidationError(fieldError.getField(), rejectedValue, fieldError.getDefaultMessage()));
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Non-Field Error found:" + error.toString());
                }
            }
        }
        response.getErrors().add(errorDetails);
        return new ResponseEntity(response, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        AvailabilityResponse response = new AvailabilityResponse();
        response.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.toString());
        response.getErrors().add(ErrorDetailsFactory.buildMediaTypeNotSupportedErrorDetails());
        return new ResponseEntity(response, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        AvailabilityResponse response = new AvailabilityResponse();
        response.setStatus(HttpStatus.BAD_REQUEST.toString());
        response.getErrors().add(ErrorDetailsFactory.buildBadRequestErrorDetails());
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

}

package com.rccl.middleware.ai.common;

/**
 * Common constant class.
 */
public class Constant {

    // Data access error messages
    public static final String QUERY_NOT_EMPTY = "query is null or empty";
    
    // HQL Queries
    public static final String HQL_GET_CAMERA_PEOPLE_COUNT = "SELECT "
            + "to_char(pc.ts, 'HH12:MI') || ':' || case when extract(second from pc.ts) < 10 then '00' "
            + "when extract(second from pc.ts) < 20 then '10' "
            + "when extract(second from pc.ts) < 30 then '20' "
            + "when extract(second from pc.ts) < 40 then '30' "
            + "when extract(second from pc.ts) < 50 then '40' "
            + "when extract(second from pc.ts) <= 59 then '50' "
            + "else '00' end || ' ' || to_char(pc.ts, 'AM') as ts, "
            + "max(pc.ts) as max_ts, "
            + "min(pc.ts) as min_ts, "
            + "avg(pc.peopleCount) as peopleCount "
            + "FROM PeopleCount pc "
            + "WHERE pc.ts >= (select max(pc.ts) - interval '2 minutes' "
                + "from PeopleCount where pc.model='nn_peoplecount' and pc.venue= :venue and pc.cameraId= :cameraId) "
            + "AND pc.model='nn_peoplecount' "
            + "AND pc.venue= :venue "
            + "AND pc.cameraId= :cameraId "
            + "group by 1 " 
            + "order by 1 asc ";
}

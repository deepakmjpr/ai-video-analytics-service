package com.rccl.middleware.ai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AiVideoTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(AiVideoTestApplication.class, args);
    }
}

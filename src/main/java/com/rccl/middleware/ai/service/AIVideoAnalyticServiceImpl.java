package com.rccl.middleware.ai.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rccl.middleware.ai.common.Constant;
import com.rccl.middleware.ai.controller.AIVideoAnalyticController;
import com.rccl.middleware.ai.data.access.CommonDataAccess;
import com.rccl.middleware.ai.exception.AIVideoAnalyticException;

@Service
public class AIVideoAnalyticServiceImpl implements AIVideoAnalyticService {

    private static Logger logger = LoggerFactory.getLogger(AIVideoAnalyticController.class);
    
    @Autowired
    private CommonDataAccess dataAccess;

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> getAveragePeopleCount(String venue, String cameraId) {

        Map<String, Object> averagePeopleCounts = new HashMap<>();
        Map<String, Object> queryParameters = new HashMap<>();
        queryParameters.put("venue", venue);
        queryParameters.put("cameraId", cameraId);

        try {
            List<Object> cameraCountResults = (List<Object>) dataAccess.getByQuery(Constant.HQL_GET_CAMERA_PEOPLE_COUNT,
                    queryParameters);
            logger.info("cameraCountResults: {}", cameraCountResults);
            averagePeopleCounts.put("camera", averagePeopleCounts);
        } catch (AIVideoAnalyticException e) {
            logger.error(e.getMessage(), e);
        }
        return averagePeopleCounts;
    }
}

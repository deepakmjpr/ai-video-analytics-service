package com.rccl.middleware.ai.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rccl.middleware.ai.model.ApplicationConfiguration;

@Service
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    private ApplicationConfiguration applicationConfiguration;
    
    @Override
    public ApplicationConfiguration getConfig() {
        return applicationConfiguration;
    }
}

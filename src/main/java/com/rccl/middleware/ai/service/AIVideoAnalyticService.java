package com.rccl.middleware.ai.service;

import java.util.Map;

/**
 * Service class for Video Analytic data.
 */
public interface AIVideoAnalyticService {
    
    /**
     * Get the average people counts based on venue and cameraId.
     * @param venue venue
     * @param cameraId camera id
     * @return average people counts map
     */
    Map<String, Object> getAveragePeopleCount(String venue, String cameraId);

}

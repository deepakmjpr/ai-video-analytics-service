package com.rccl.middleware.ai.service;

import com.rccl.middleware.ai.model.ApplicationConfiguration;

public interface ConfigService {

    ApplicationConfiguration getConfig();
}

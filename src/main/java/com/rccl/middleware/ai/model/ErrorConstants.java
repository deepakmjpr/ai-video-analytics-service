package com.rccl.middleware.ai.model;

import lombok.Data;

public final class ErrorConstants {

    @Data
    public static final class BadRequest {
        public static final String errorTitle = "Invalid Request";
        public static final String developerMessage = "Request body did not pass validation, "
                + "see validation errors for details.";
        public static final String userMessage = "The request body is improper";
        public static final String internalMessage = "Request Body did not pass input validation";
        public static final String errorCode = "request.body.invalid";
        public static final String moreInfo = "";
    }

    @Data
    public static final class InvalidRequest {
        public static final String errorTitle = "Invalid Request";
        public static final String developerMessage = "Request body did not pass validation, "
                + "see validation errors for details.";
        public static final String userMessage = "The request body is improper";
        public static final String internalMessage = "Request Body did not pass input validation";
        public static final String errorCode = "request.parameters.invalid";
        public static final String moreInfo = "";
    }

    @Data
    public static final class PersistenceException {
        public static final String errorTitle = "Persistence Exception";
        public static final String developerMessage = "Error reading data from SQL Server";
        public static final String userMessage = "Error reading data from database";
        public static final String internalMessage = "Error reading data from SQL Server";
        public static final String errorCode = "sql.server.exception";
        public static final String moreInfo = "Common cause can be an invalid sailDate ex. 20180231";
    }

    @Data
    public static final class NoSilverWhereOfferingsFound {
        public static final String errorTitle = "No Offerings Found";
        public static final String developerMessage = "No offerings found for the given criteria.";
        public static final String userMessage = "Sorry, no offerings found!";
        public static final String internalMessage = "No offerings returned from backend reservation system.";
        public static final String errorCode = "dining.offerings.not.found";
        public static final String moreInfo = "Please ensure you're using a valid productCode and that there is "
                + "inventory for the requested days.";
    }

    @Data
    public static final class NoPCPOfferingFound {
        public static final String errorTitle = "Matching offering not found";
        public static final String developerMessage = "Matching offering could not be found";
        public static final String userMessage = "Sorry, could not find an offering matching the requested date "
                + "and time.";
        public static final String internalMessage = "Matching offering does not exist in database.";
        public static final String errorCode = "pcp.matching.offering.not.found";
        public static final String moreInfo = "No PCP offering found for :";
    }

    @Data
    public static final class NoPCPOfferingsFound {
        public static final String errorTitle = "No Offerings Found";
        public static final String developerMessage = "No Offerings Found ";
        public static final String userMessage = "Sorry, could not find any offerings.";
        public static final String internalMessage = "No offerings found in database for the given criteria";
        public static final String errorCode = "pcp.offerings.not.found";
        public static final String moreInfo = "Please ensure you're using a valid shipCode, sailDate and productCode.";
    }

    @Data
    public static final class UnsupportedMediaType {
        public static final String errorTitle = "Unsupported Media Type";
        public static final String developerMessage = "Media type is unsupported.";
        public static final String userMessage = "Sorry, that media type is not supported.";
        public static final String internalMessage = "Media type is unsupported.";
        public static final String errorCode = "unsupported.media.type";
        public static final String moreInfo = "Please ensure you're using a valid media type.";
    }

}

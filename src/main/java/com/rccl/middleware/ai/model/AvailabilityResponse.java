package com.rccl.middleware.ai.model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AvailabilityResponse {

    private String status = "200";
    
    private List<ErrorDetails> errors = new ArrayList<>();
    
//    /**
//     * @return the status
//     */
//    public String getStatus() {
//        return status;
//    }
//
//    /**
//     * @param status the status to set
//     */
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    /**
//     * @return the errors
//     */
//    public List<ErrorDetails> getErrors() {
//        return errors;
//    }
//
//    /**
//     * @param errors the errors to set
//     */
//    public void setErrors(List<ErrorDetails> errors) {
//        this.errors = errors;
//    }
}

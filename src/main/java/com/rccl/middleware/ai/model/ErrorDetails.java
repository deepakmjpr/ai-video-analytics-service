package com.rccl.middleware.ai.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorDetails {

    private String errorTitle;
    private String developerMessage;
    private String userMessage;
    private String internalMessage;
    private String errorCode;
    private String moreInfo;
    private List<ValidationError> validationErrors = new ArrayList<>();

//    /**
//     * @return the errorTitle
//     */
//    public String getErrorTitle() {
//        return errorTitle;
//    }
//
//    /**
//     * @param errorTitle the errorTitle to set
//     */
//    public void setErrorTitle(String errorTitle) {
//        this.errorTitle = errorTitle;
//    }
//
//    /**
//     * @return the developerMessage
//     */
//    public String getDeveloperMessage() {
//        return developerMessage;
//    }
//
//    /**
//     * @param developerMessage the developerMessage to set
//     */
//    public void setDeveloperMessage(String developerMessage) {
//        this.developerMessage = developerMessage;
//    }
//
//    /**
//     * @return the userMessage
//     */
//    public String getUserMessage() {
//        return userMessage;
//    }
//
//    /**
//     * @param userMessage the userMessage to set
//     */
//    public void setUserMessage(String userMessage) {
//        this.userMessage = userMessage;
//    }
//
//    /**
//     * @return the internalMessage
//     */
//    public String getInternalMessage() {
//        return internalMessage;
//    }
//
//    /**
//     * @param internalMessage the internalMessage to set
//     */
//    public void setInternalMessage(String internalMessage) {
//        this.internalMessage = internalMessage;
//    }
//
//    /**
//     * @return the errorCode
//     */
//    public String getErrorCode() {
//        return errorCode;
//    }
//
//    /**
//     * @param errorCode the errorCode to set
//     */
//    public void setErrorCode(String errorCode) {
//        this.errorCode = errorCode;
//    }
//
//    /**
//     * @return the moreInfo
//     */
//    public String getMoreInfo() {
//        return moreInfo;
//    }
//
//    /**
//     * @param moreInfo the moreInfo to set
//     */
//    public void setMoreInfo(String moreInfo) {
//        this.moreInfo = moreInfo;
//    }
//
//    /**
//     * @return the validationErrors
//     */
//    public List<ValidationError> getValidationErrors() {
//        return validationErrors;
//    }
//
//    /**
//     * @param validationErrors the validationErrors to set
//     */
//    public void setValidationErrors(List<ValidationError> validationErrors) {
//        this.validationErrors = validationErrors;
//    }
}

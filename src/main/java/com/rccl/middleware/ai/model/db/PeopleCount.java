package com.rccl.middleware.ai.model.db;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(name = "people_count")
public class PeopleCount implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    private Date ts;
    
    @Id
    @Column(name = "camera_id")
    private String cameraId;
    
    @Id
    private String venue;

    @Id
    private String model;

    @Column(name = "people_count")
    private Integer peopleCount;
}

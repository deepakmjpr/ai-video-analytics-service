package com.rccl.middleware.ai.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfiguration {

    @Value("${silverwhere.uri}")
    private String silverWhereUri;
    
    @Value("${spring.datasource.url}")
    private String springDatasourceUrl;
    
    @Value("${spring.datasource.username}")
    private String springDatasourceUsername;
    
    @Value("${spring.datasource.pass-word}")
    private String springDatasourcePassword;
    
    @Value("${spring.datasource.driver-class-name}")
    private String springDatasourceDriverClassName;
    
    @Value("${logging.level.root}")
    private String loggingLevelRoot;

    @Value("${logging.level.org.springframework.web}")
    private String loggingLevelOrgSpringframeworkWeb;
    
    @Value("${logging.level.com.rccl.digital.commerce}")
    private String loggingLevelcomRcclDigitalCommerce;

    public String getSilverWhereUri() {
        return silverWhereUri;
    }

    public void setSilverWhereUri(String silverWhereUri) {
        this.silverWhereUri = silverWhereUri;
    }

    public String getSpringDatasourceUrl() {
        return springDatasourceUrl;
    }

    public void setSpringDatasourceUrl(String springDatasourceUrl) {
        this.springDatasourceUrl = springDatasourceUrl;
    }

    public String getSpringDatasourceUsername() {
        return springDatasourceUsername;
    }

    public void setSpringDatasourceUsername(String springDatasourceUsername) {
        this.springDatasourceUsername = springDatasourceUsername;
    }

    public String getSpringDatasourcePassword() {
        return springDatasourcePassword;
    }

    public void setSpringDatasourcePassword(String springDatasourcePassword) {
        this.springDatasourcePassword = springDatasourcePassword;
    }

    public String getSpringDatasourceDriverClassName() {
        return springDatasourceDriverClassName;
    }

    public void setSpringDatasourceDriverClassName(String springDatasourceDriverClassName) {
        this.springDatasourceDriverClassName = springDatasourceDriverClassName;
    }

    public String getLoggingLevelRoot() {
        return loggingLevelRoot;
    }

    public void setLoggingLevelRoot(String loggingLevelRoot) {
        this.loggingLevelRoot = loggingLevelRoot;
    }

    public String getLoggingLevelOrgSpringframeworkWeb() {
        return loggingLevelOrgSpringframeworkWeb;
    }

    public void setLoggingLevelOrgSpringframeworkWeb(String loggingLevelOrgSpringframeworkWeb) {
        this.loggingLevelOrgSpringframeworkWeb = loggingLevelOrgSpringframeworkWeb;
    }

    public String getLoggingLevelcomRcclDigitalCommerce() {
        return loggingLevelcomRcclDigitalCommerce;
    }

    public void setLoggingLevelcomRcclDigitalCommerce(String loggingLevelcomRcclDigitalCommerce) {
        this.loggingLevelcomRcclDigitalCommerce = loggingLevelcomRcclDigitalCommerce;
    }
}

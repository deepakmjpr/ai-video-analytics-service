package com.rccl.middleware.ai.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ValidationError {
    private String element;
    private String invalidValue;
    private String error;

//    /**
//     * Parametrized constructor
//     * @param element
//     * @param invalidValue
//     * @param error
//     */
//    public ValidationError(String element, String invalidValue, String error) {
//        this.element = element;
//        this.invalidValue = invalidValue;
//        this.error = error;
//    }

//    /**
//     * @return the element
//     */
//    public String getElement() {
//        return element;
//    }
//
//    /**
//     * @param element the element to set
//     */
//    public void setElement(String element) {
//        this.element = element;
//    }
//
//    /**
//     * @return the invalidValue
//     */
//    public String getInvalidValue() {
//        return invalidValue;
//    }
//
//    /**
//     * @param invalidValue the invalidValue to set
//     */
//    public void setInvalidValue(String invalidValue) {
//        this.invalidValue = invalidValue;
//    }
//
//    /**
//     * @return the error
//     */
//    public String getError() {
//        return error;
//    }
//
//    /**
//     * @param error the error to set
//     */
//    public void setError(String error) {
//        this.error = error;
//    }
}

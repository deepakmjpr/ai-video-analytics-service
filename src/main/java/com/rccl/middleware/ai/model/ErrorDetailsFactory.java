package com.rccl.middleware.ai.model;

public class ErrorDetailsFactory {

    public static ErrorDetails buildInvalidParametErrorDetails() {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setErrorTitle(ErrorConstants.InvalidRequest.errorTitle);
        errorDetails.setDeveloperMessage(ErrorConstants.InvalidRequest.developerMessage);
        errorDetails.setUserMessage(ErrorConstants.InvalidRequest.userMessage);
        errorDetails.setInternalMessage(ErrorConstants.InvalidRequest.internalMessage);
        errorDetails.setErrorCode(ErrorConstants.InvalidRequest.errorCode);
        errorDetails.setMoreInfo(ErrorConstants.InvalidRequest.moreInfo);

        return errorDetails;
    }

    public static ErrorDetails buildPersistenceExceptionErrorDetails() {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setErrorTitle(ErrorConstants.PersistenceException.errorTitle);
        errorDetails.setDeveloperMessage(ErrorConstants.PersistenceException.developerMessage);
        errorDetails.setUserMessage(ErrorConstants.PersistenceException.userMessage);
        errorDetails.setInternalMessage(ErrorConstants.PersistenceException.internalMessage);
        errorDetails.setErrorCode(ErrorConstants.PersistenceException.errorCode);
        errorDetails.setMoreInfo(ErrorConstants.PersistenceException.moreInfo);

        return errorDetails;
    }

    public static ErrorDetails buildNoSilverWhereOfferingsFoundErrorDetails() {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setErrorTitle(ErrorConstants.NoSilverWhereOfferingsFound.errorTitle);
        errorDetails.setDeveloperMessage(ErrorConstants.NoSilverWhereOfferingsFound.developerMessage);
        errorDetails.setUserMessage(ErrorConstants.NoSilverWhereOfferingsFound.userMessage);
        errorDetails.setInternalMessage(ErrorConstants.NoSilverWhereOfferingsFound.internalMessage);
        errorDetails.setErrorCode(ErrorConstants.NoSilverWhereOfferingsFound.errorCode);
        errorDetails.setMoreInfo(ErrorConstants.NoSilverWhereOfferingsFound.moreInfo);

        return errorDetails;
    }

    public static ErrorDetails buildNoPCPOfferingsFoundErrorDetails() {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setErrorTitle(ErrorConstants.NoPCPOfferingsFound.errorTitle);
        errorDetails.setDeveloperMessage(ErrorConstants.NoPCPOfferingsFound.developerMessage);
        errorDetails.setUserMessage(ErrorConstants.NoPCPOfferingsFound.userMessage);
        errorDetails.setInternalMessage(ErrorConstants.NoPCPOfferingsFound.internalMessage);
        errorDetails.setErrorCode(ErrorConstants.NoPCPOfferingsFound.errorCode);
        errorDetails.setMoreInfo(ErrorConstants.NoPCPOfferingsFound.moreInfo);

        return errorDetails;
    }

    public static ErrorDetails buildBadRequestErrorDetails() {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setErrorTitle(ErrorConstants.BadRequest.errorTitle);
        errorDetails.setDeveloperMessage(ErrorConstants.BadRequest.developerMessage);
        errorDetails.setUserMessage(ErrorConstants.BadRequest.userMessage);
        errorDetails.setInternalMessage(ErrorConstants.BadRequest.internalMessage);
        errorDetails.setErrorCode(ErrorConstants.BadRequest.errorCode);
        errorDetails.setMoreInfo(ErrorConstants.BadRequest.moreInfo);

        return errorDetails;
    }

    public static ErrorDetails buildMediaTypeNotSupportedErrorDetails() {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setErrorTitle(ErrorConstants.UnsupportedMediaType.errorTitle);
        errorDetails.setDeveloperMessage(ErrorConstants.UnsupportedMediaType.developerMessage);
        errorDetails.setUserMessage(ErrorConstants.UnsupportedMediaType.userMessage);
        errorDetails.setInternalMessage(ErrorConstants.UnsupportedMediaType.internalMessage);
        errorDetails.setErrorCode(ErrorConstants.UnsupportedMediaType.errorCode);
        errorDetails.setMoreInfo(ErrorConstants.UnsupportedMediaType.moreInfo);

        return errorDetails;
    }
}

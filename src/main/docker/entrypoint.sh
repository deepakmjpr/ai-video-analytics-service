#!/bin/sh

JAVA_OPTS="-Xms128m -Xmx1024m"

APP_CLASSPATH=/usr/src/app/inventory/lib/*


JAR_PATH=$(ls $APP_CLASSPATH | grep com.rccl.middleware.ai)
echo "JAR_PATH"
exec java $JAVA_OPTS -jar $JAR_PATH
